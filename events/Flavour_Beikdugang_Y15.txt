namespace = flavor_beikdugang

#First Encounter with Federation
country_event = {
	id = flavor_beikdugang.1
	title = flavor_beikdugang.1.t
	desc = {
		trigger = { has_leader = "Houzhao Targuun" }
		desc = "flavor_beikdugang.1.d1"
	}
	desc = {
		trigger = { NOT = { has_leader = "Houzhao Targuun" } }
		desc = "flavor_beikdugang.1.d2"
	}
	picture = DIPLOMACY_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = flavor_beikdugang.1.a
		ai_chance = { factor = 100 }
		forbidden_lands_superregion = {
			discover_country = ROOT
		}
		add_republican_tradition = 10
		add_prestige = 25
		add_splendor = 50
		add_historical_friend = J02
		J02 = { add_historical_friend = ROOT }
		if = {
			limit = { has_leader = "Houzhao Targuun" }
			kill_leader = { type = "Houzhao Targuun" }
			add_prestige = 25
			add_splendor = 50
		}
	}
}

#Fall of Tianlou
country_event = {
	id = flavor_beikdugang.2
	title = flavor_beikdugang.2.t
	desc = flavor_beikdugang.2.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = flavor_beikdugang.2.a
		ai_chance = { factor = 33 }
		4871 = { #Tianlou
			add_base_tax = -1
			add_base_production = -1
			add_base_manpower = -1
			add_devastation = 5
		}
		4907 = { #Beikdugang
			add_base_tax = 1
			add_base_production = 1
			add_base_manpower = 1
		}
	}
	option = {
		name = flavor_beikdugang.2.b
		ai_chance = { factor = 50 }
		4871 = {
			add_base_tax = -4
			add_base_production = -4
			add_base_manpower = -4
			add_devastation = 25
		}
		4907 = {
			add_base_tax = 3
			add_base_production = 3
			add_base_manpower = 3
		}
	}
	option = {
		name = flavor_beikdugang.2.c
		ai_chance = { factor = 17 }
		4871 = {
			add_base_tax = -6
			add_base_production = -6
			add_base_manpower = -6
			add_devastation = 50
		}
		4907 = {
			add_base_tax = 5
			add_base_production = 5
			add_base_manpower = 5
		}
	}
}